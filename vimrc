" Fix backspace
set backspace=2

" Syntax Highlighting
set nocompatible
filetype on
filetype plugin indent on
syntax enable
syntax on
set grepprg=grep\ -nH\ $*

" Automatic Indentation
set autoindent

" Tabs
set expandtab
set smarttab
set shiftwidth=4
set softtabstop=4

" Line Numbers
set number

set background=dark

" lines, cols in status line
set ruler
set rulerformat=%=%h%m%r%w\ %(%c%V%),%l/%L\ %P

" arrow keys, bs, space wrap to next/prev line
set whichwrap=b,s,<,>,[,]

" Tab navigation like firefox
"nnoremap <C-S-tab> :tabprevious<CR>
"nnoremap <C-tab>   :tabnext<CR>
"nnoremap <C-t>     :tabnew<CR>
"inoremap <C-S-tab> <Esc>:tabprevious<CR>i
"inoremap <C-tab>   <Esc>:tabnext<CR>i
"inoremap <C-t>     <Esc>:tabnew<CR>

" Always open files in new tabs
" autocmd VimEnter * tab all
" autocmd BufAdd * exe 'tablast | tabe "' . expand( "<afile") .'"'

"set background=dark
"colorscheme solarized

" Tag Bar
nmap <F8> :TagbarToggle<CR>

" Syntastic
let g:syntastic_check_on_open=1
let g:syntastic_auto_loc_list=1
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_php_checkers=['php', 'phpcs']
let g:syntastic_javascript_checkers = ['eslint']

"Pathogen setup
execute pathogen#infect()

" Highlight whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
"autocmd BufWinLeave * call clearmatches()

" Use tabs in makefiles
autocmd FileType make setlocal noexpandtab

" 4 space indentation for HTML, CSS and Javascript
autocmd FileType html setlocal shiftwidth=4 tabstop=4
autocmd FileType scss setlocal shiftwidth=4 tabstop=4
autocmd FileType javascript setlocal shiftwidth=4 tabstop=4

" 2 space indentation for Ruby files
autocmd FileType ruby setlocal shiftwidth=2 tabstop=2

" Use Python 3 for linting
let g:pymode_python = 'python3'
